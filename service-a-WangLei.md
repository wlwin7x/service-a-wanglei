

  # 1. 为该服务开发Dockerfile，构建容器镜像并发布到镜像仓库（地址不限）

#DockerFile

    #源镜像
    FROM golang:latest
    #将service-a拷贝到镜像中
    ADD ./service-a $GOPATH/
    #CMD启动镜像时启动service
    CMD ./service-a --file config.yaml
    #暴露端口
    EXPOSE 8888


#临时登录凭证

    docker login test-service-a.tencentcloudcr.com --username 100023949554 --password eyJhbGciOiJSUzI1NiIsImtpZCI6IldWVkM6SkhRNzpORElEOjRLVUQ6WEJTQTpSQTVJOkdNNzM6Q1dUSDpFM1NHOkZFVjM6TExWRDpSWldEIn0.eyJvd25lclVpbiI6IjEwMDAyMzk0OTU1NCIsIm9wZXJhdG9yVWluIjoiMTAwMDIzOTQ5NTU0IiwiZXhwIjoxNjQ1NzIyMjk2LCJuYmYiOjE2NDU3MTg2OTYsImlhdCI6MTY0NTcxODY5Nn0.d4O3kXB6ZulPi0TWaXyrRVbwbRqxUBTLK80ILbqGP0LKIimfUZH8iL30sJ55S0Y3lVhrLFAuY6S6jlFDGG7Dh8i3awCTm9tn-u5b707uQaZycN-GN1o2g-p2llGW_F9DBt8ReR7tbV2Glt6oNjKUI3pYVhymIqCUB8LYQ4G69Pa2I2uyqm7CkG853UbeG6Oc8hG--Q8S3ceo7lQysCOHFEPaoqKqiPhwN07Oa3x98B2qv6Ls6HgXR6_6jdelzy9Z6q7uCUehn-0LOZJQ7XAKe-RnIWLYGrxvQav_Uqs2JEK2l91_laU6MAjZPPCfHrvy54KBQu5-ulKERDLw7vKO_g

#pull镜像到本地

    docker pull test-service-a.tencentcloudcr.com/service-a-wanglei/service-a-wanglei:wanglei

#运行镜像

    docker run -itd -p 8888:8888 test-service-a.tencentcloudcr.com/service-a-wanglei/service-a-wanglei:wanglei

#访问测试

    curl http://localhost:8888/healthz

    curl http://localhost:8888/api/v1/features



  # 2. ⽤Terraform代码实现创建腾讯云TKE（实现代码即可）

#环境变量设置好TENCENTCLOUD_SECRET_ID，TENCENTCLOUD_SECRET_KEY，可以在https://console.cloud.tencent.com/cam/capi 生成，也可以在tf文件中明文，但是不安全最好采取变量。

    export TENCENTCLOUD_SECRET_ID="AKID4yd18yYqrb5IEtHPGrxxxxx"

    export TENCENTCLOUD_SECRET_KEY="XnmUv60dz5uRhhSw95lxxxxxx"


#main.tf

    #名字标识

    variable "name" {
      default = "service-a-wanglei"
    }

    #地区

    variable "region" {
      default = "ap-beijing"
    }
    #k8s 版本

    variable "k8s_ver" {
      default = "1.18.4"
    }

    # pod ip 地址段
    variable "pod_ip_seg" {
      default = "192.168"
    }

    # vpc ip 地址段
    variable "vpc_ip_seg" {
      default = "10.10"
    }

    # 机型
    variable "default_instance_type" {
      default = "S2.MEDIUM4"
    }

    # node 密码
    variable "node_password" {
      default = "123.ComCom"
    }




    terraform {
      required_providers {
        tencentcloud = {
          source = "tencentcloudstack/tencentcloud"
        }
      }
    }

    # 指定腾讯云和其大区
    provider "tencentcloud" {
      region = var.region
    }

    # 定义安全组
    resource "tencentcloud_security_group" "sg01" {
      name        = "${var.name}-sg"
      description = "${var.name} security group @ powered by terraform"
    }

    resource "tencentcloud_security_group_lite_rule" "sg01-rule" {
      security_group_id = tencentcloud_security_group.sg01.id

      ingress = [
        "ACCEPT#0.0.0.0/0#ALL#ICMP",
        "ACCEPT#0.0.0.0/0#22#TCP",
        "ACCEPT#0.0.0.0/0#30000-32768#TCP",
        "ACCEPT#0.0.0.0/0#30000-32768#UDP",
      ]

      egress = [
        "ACCEPT#0.0.0.0/0#ALL#ALL",
      ]
    }


    # 查询当前可用区, 将设置到节点池
    data "tencentcloud_availability_zones" "all_zones" {
    }

    # 定义一个 VPC 网络
    resource "tencentcloud_vpc" "vpc01" {
      name         = "${var.name}-01"
      cidr_block   = "${var.vpc_ip_seg}.0.0/16"
      is_multicast = false

      tags = {
        "user" = var.name
      }
    }

    # 定义子网，这里会给每个 zone 定义一个子网
    resource "tencentcloud_subnet" "subset01" {
      count             = length(data.tencentcloud_availability_zones.all_zones.zones)
      name              = "${var.name}-subset-${count.index}"
      vpc_id            = tencentcloud_vpc.vpc01.id
      availability_zone = data.tencentcloud_availability_zones.all_zones.zones[count.index].name
      cidr_block        = "${var.vpc_ip_seg}.${count.index}.0/24"
      is_multicast      = false
      tags = {
        "user" = var.name
      }
    }


    # 创建 TKE 集群
    resource "tencentcloud_kubernetes_cluster" "tke_managed" {
      vpc_id                                     = tencentcloud_vpc.vpc01.id
      cluster_version                            = var.k8s_ver
      cluster_cidr                               = "${var.pod_ip_seg}.0.0/16"
      cluster_max_pod_num                        = 64
      cluster_name                               = "${var.name}-tke-01"
      cluster_desc                               = "created by terraform"
      cluster_max_service_num                    = 2048
      cluster_internet                           = true
      managed_cluster_internet_security_policies = ["0.0.0.0/0"]
      cluster_deploy_type                        = "MANAGED_CLUSTER"
      cluster_os                                 = "tlinux2.4x86_64"
      container_runtime                          = "containerd" #容器类型，也可以使用docker
      deletion_protection                        = false

      worker_config {
        instance_name              = "${var.name}-node"
        availability_zone          = data.tencentcloud_availability_zones.all_zones.zones[0].name
        instance_type              = var.default_instance_type
        system_disk_type           = "CLOUD_SSD"
        system_disk_size           = 50
        internet_charge_type       = "TRAFFIC_POSTPAID_BY_HOUR"
        internet_max_bandwidth_out = 1
        public_ip_assigned         = true  #是否分配公网ip “true” “false”
        subnet_id                  = tencentcloud_subnet.subset01[0].id
        security_group_ids         = [tencentcloud_security_group.sg01.id]

        enhanced_security_service = false
        enhanced_monitor_service  = false
        password                  = var.node_password
      }


      labels = {
        "user" = var.name
      }
    }

    #  创建一个节点池
    resource "tencentcloud_kubernetes_node_pool" "node-pool" {
      name                 = "${var.name}-pool"
      cluster_id           = tencentcloud_kubernetes_cluster.tke_managed.id
      max_size             = 10
      min_size             = 0
      vpc_id               = tencentcloud_vpc.vpc01.id
      subnet_ids           = [for s in tencentcloud_subnet.subset01 : s.id]
      retry_policy         = "INCREMENTAL_INTERVALS"
      desired_capacity     = 0
      enable_auto_scale    = true
      delete_keep_instance = false
      node_os              = "tlinux2.4x86_64"

      auto_scaling_config {
        instance_type      = var.default_instance_type
        system_disk_type   = "CLOUD_PREMIUM"
        system_disk_size   = "50"
        security_group_ids = [tencentcloud_security_group.sg01.id]

        data_disk {
          disk_type = "CLOUD_PREMIUM"
          disk_size = 50
        }

        internet_charge_type       = "TRAFFIC_POSTPAID_BY_HOUR"
        internet_max_bandwidth_out = 5
        public_ip_assigned         = true
        password                   = var.node_password
        enhanced_security_service  = false
        enhanced_monitor_service   = false

      }

      labels = {
        "user" = var.name,
      }

    }

    #导出kubeconfig 在腾讯TKE集群控制台也可看到
    output "KUBECONFIG" {
      description = "下面的配置是 kubeconfig，请拷贝并妥善存储"
      value       = tencentcloud_kubernetes_cluster.tke_managed.kube_config
    }




#初始化项目

    terraform init

#执行tf脚本

    terraform apply -auto-approve




  # 3. 开发Helm chart，将该服务部署到部署到指定的TKE集群（TKE访问凭证将以附件抄送，注意该凭证只能访问 service-USERNAME 这个Namespace，请查看⽂件第⼀⾏）

#已提交到指定TKE集群及指定namespace

    [root@VM-0-10-centos opt]# kubectl -n service-wanglei get deploy,svc,pod
    NAME                              READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/service-wanglei   1/1     1            1           125m

    NAME                      TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)    AGE
    service/service-wanglei   ClusterIP   10.0.255.26   <none>        8888/TCP   125m

    NAME                                  READY   STATUS    RESTARTS   AGE
    pod/service-wanglei-6688b8568-kt9lm   1/1     Running   0          125m

    root@service-wanglei-6688b8568-kt9lm:/go# curl http://localhost:8888/healthz
    {"message":"Service a is running well","status":"OK"}
    
    root@service-wanglei-6688b8568-kt9lm:/go# curl http://localhost:8888/api/v1/features
    {"Env":"test","Port":"8888","FeatureOne":true,"FeatureTwo":false}

    
#Helm Chart

    #提交当前目录项目至namespace：service-wanglei
    helm -n service-wanglei install service-wanglei .    

#Chart.yaml

    apiVersion: v2
    name: service-wanglei
    description: A Helm chart for Kubernetes

    # A chart can be either an 'application' or a 'library' chart.
    #
    # Application charts are a collection of templates that can be packaged into versioned archives
    # to be deployed.
    #
    # Library charts provide useful utilities or functions for the chart developer. They're included as
    # a dependency of application charts to inject those utilities and functions into the rendering
    # pipeline. Library charts do not define any templates and therefore cannot be deployed.
    type: application

    # This is the chart version. This version number should be incremented each time you make changes
    # to the chart and its templates, including the app version.
    # Versions are expected to follow Semantic Versioning (https://semver.org/)
    version: 0.1.0

    # This is the version number of the application being deployed. This version number should be
    # incremented each time you make changes to the application. Versions are not expected to
    # follow Semantic Versioning. They should reflect the version the application is using.
    # It is recommended to use it with quotes.
    appVersion: "1.16.0"


    maintainers:
    - email: wlwin_7z@126.com
      name: WangLei
  
    
#values.yaml

    # Default values for service-wanglei.
    # This is a YAML-formatted file.
    # Declare variables to be passed into your templates.

    replicaCount: 1

    image:
      repository: test-service-a.tencentcloudcr.com/service-a-wanglei/service-a-wanglei
      pullPolicy: IfNotPresent
      # Overrides the image tag whose default is the chart appVersion.
      tag: wanglei

    imagePullSecrets: []
    nameOverride: ""
    fullnameOverride: ""

    serviceAccount:
      # Specifies whether a service account should be created
      create: true
      # Annotations to add to the service account
      annotations: {}
      # The name of the service account to use.
      # If not set and create is true, a name is generated using the fullname template
      name: ""

    podAnnotations: {}

    podSecurityContext: {}
      # fsGroup: 2000

    securityContext: {}
      # capabilities:
      #   drop:
      #   - ALL
      # readOnlyRootFilesystem: true
      # runAsNonRoot: true
      # runAsUser: 1000

    service:
      type: ClusterIP
      port: 8888

    ingress:
      enabled: false
      className: ""
      annotations: {}
        # kubernetes.io/ingress.class: nginx
        # kubernetes.io/tls-acme: "true"
      hosts:
        - host: chart-example.local
          paths:
            - path: /
              pathType: ImplementationSpecific
      tls: []
      #  - secretName: chart-example-tls
      #    hosts:
      #      - chart-example.local

    resources: {}
      # We usually recommend not to specify default resources and to leave this as a conscious
      # choice for the user. This also increases chances charts run on environments with little
      # resources, such as Minikube. If you do want to specify resources, uncomment the following
      # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
      # limits:
      #   cpu: 100m
      #   memory: 128Mi
      # requests:
      #   cpu: 100m
      #   memory: 128Mi

    autoscaling:
      enabled: false
      minReplicas: 1
      maxReplicas: 100
      targetCPUUtilizationPercentage: 80
      # targetMemoryUtilizationPercentage: 80

    nodeSelector: {}

    tolerations: []

    affinity: {}




#deployment.yaml

    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: {{ include "service-wanglei.fullname" . }}
      labels:
        {{- include "service-wanglei.labels" . | nindent 4 }}
    spec:
      {{- if not .Values.autoscaling.enabled }}
      replicas: {{ .Values.replicaCount }}
      {{- end }}
      selector:
        matchLabels:
          {{- include "service-wanglei.selectorLabels" . | nindent 6 }}
      template:
        metadata:
          {{- with .Values.podAnnotations }}
          annotations:
            {{- toYaml . | nindent 8 }}
          {{- end }}
          labels:
            {{- include "service-wanglei.selectorLabels" . | nindent 8 }}
        spec:
          {{- with .Values.imagePullSecrets }}
          imagePullSecrets:
            {{- toYaml . | nindent 8 }}
          {{- end }}
          serviceAccountName: {{ include "service-wanglei.serviceAccountName" . }}
          securityContext:
            {{- toYaml .Values.podSecurityContext | nindent 8 }}
          containers:
            - name: {{ .Chart.Name }}
              securityContext:
                {{- toYaml .Values.securityContext | nindent 12 }}
              image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
              imagePullPolicy: {{ .Values.image.pullPolicy }}
              ports:
                - name: http
                  containerPort: 8888 #服务暴露端口
                  protocol: TCP
              resources:
                {{- toYaml .Values.resources | nindent 12 }}
          {{- with .Values.nodeSelector }}
          nodeSelector:
            {{- toYaml . | nindent 8 }}
          {{- end }}
          {{- with .Values.affinity }}
          affinity:
            {{- toYaml . | nindent 8 }}
          {{- end }}
          {{- with .Values.tolerations }}
          tolerations:
            {{- toYaml . | nindent 8 }}
          {{- end }}

